# Avada Kedavra
### Gulp template using Fractal for style guide generation. Used in conjunction with Perch. 

Current Build: Beta 1.0

## Folder Structure

```
├── assets/
│   ├── css
│   ├── fonts
│	├──	images
│	├──	js
│	│	├──	helpers
│	│	├──	plugins
│	│	├──	src
│	│	└──	vendor
│	└──	sass
│		├──	base
		├── modules
		├── partials
│		└──	global
├── components/
│	├── 01-units
│	└──	
├── docs/
│	└──	
├── .gitignore
├── bower.json
├── package.json
├── gulpfile.js
└── index.html
```

## Installation:

TBD
