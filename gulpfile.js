'use strict';
// ==> Get Gulp
const gulp        = require('gulp');

// ==> Plugins
const gutil       = require('gulp-util');
const sass        = require('gulp-sass');
const notify      = require("gulp-notify");
const nano        = require('gulp-cssnano');
const autoprefix  = require('gulp-autoprefixer');
const rename      = require("gulp-rename");
const sassGlob    = require('gulp-sass-glob');
const imagemin    = require('gulp-imagemin');
const cache       = require('gulp-cache');
const concat      = require('gulp-concat');
const uglify      = require('gulp-uglify');
const del         = require('del');
const plumber     = require('gulp-plumber');
const connect     = require('gulp-connect');
const pkg         = require('./package.json');
const browserSync = require("browser-sync").create();
const reload      = browserSync.reload;

// ==> WIP coming soon
// const fractal  = require('./fractal.js'); 
// const logger = fractal.cli.console;

// ==> Directories
var paths = {
    css: {
        src:  './src/sass/**/*.scss',
        main: './src/sass/build.scss',
        dest: './dist/css',
        maps: '/scss'
    },
    dest: './dist',
    html: {
        src: './*.html',
        dest: './public'
    },
    js: {
        src:  './src/js/**/*.js',
        main: './src/js/main.js',
        dest: './dist/js',
    }
};

// gulp.task('connect', function() {
//   connect.server({
//     root: './',
//     livereload: true
//   });
// });

// gulp.task('html', function () {
//   gulp.src(paths.html.src)
//     .pipe(connect.reload());
// });

// gulp.task('sass-watch', ['sass'], function (done) {
//     browserSync.reload();
//     done();
// });

// ==> Server
gulp.task('serve', ['sass', 'nanocss'], function() {

    browserSync.init({
        proxy: "drcpartners.local"
    });

    gulp.watch("src/sass/*.scss", ['sass']);
    gulp.watch("src/sass/**/*.scss", ['sass']);
    gulp.watch("*.html").on('change', browserSync.reload);
    gulp.watch("dist/js/*.js").on('change', browserSync.reload);
    gulp.watch("dist/css/build.min.css").on('change', browserSync.reload);
});

// ==> Error handling
function handleError(err) {
    gutil.log(err.toString());
    this.emit('end');
}

gulp.task('images', function(){
  return gulp.src('src/images/**/*.+(png|jpg|gif|svg)')
  .pipe(cache(imagemin({
      interlaced: true
   })))
  .pipe(gulp.dest('dist/images'))
});

// ==> Compile SCSS
gulp.task('sass', function() {
  return gulp.src('src/sass/build.scss')
    .pipe(plumber({ errorHandler: handleError }))
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(autoprefix('last 2 version'))
    .pipe(rename('build.css'))
    .on('error', err => console.log(err.message))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream())
    .pipe(notify({message: 'SCSS Processed!'}));
});

// ==> clean up css with nano
gulp.task('nanocss', ['sass'], function() {
    return gulp.src('dist/css/build.css')
        .pipe(nano({discardComments: {removeAll: true}}))
        .pipe(rename('build.min.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(notify({message: 'CSS Nanofied!'}));
});

// ==> Scripts
gulp.task('scripts', function() {
  return gulp.src(['./src/js/vendor/jquery.js', './src/js/plugins/*.js', './src/js/main/*.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('default', ['sass', 'nanocss', 'scripts', 'serve', 'watch']);

gulp.task('watch', function () {
    gulp.watch(paths.css.src, ['sass', 'nanocss']);
    gulp.watch(paths.js.src, ['scripts']);
    // gulp.watch(paths.html.src, ['html']);
});
